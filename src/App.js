import "./App.css";
import Calculator from "./Container/Calculator/Calculator";

const App = () => {
  return (
    <div className="App">
      <Calculator />
    </div>
  );
};

export default App;

const initialState = {
  inputValue: "",
  numbers: [0, "+", "-", 1, 2, 3, "*", 4, 5, 6, "/", 7, 8, 9, "."],
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case "NUMBER":
      if (state.inputValue === "" && action.value === 0) {
        return { ...state, inputValue: state.inputValue + action.value + "." };
      }
      return { ...state, inputValue: state.inputValue + action.value };
    case "EQUALLY":
      return { ...state, inputValue: eval(state.inputValue) };
    case "CHANGE":
      return { ...state, inputValue: action.value };
    case "CLEAR":
      return { ...state, inputValue: "" };
    default:
      return state;
  }
};
export default reducer;

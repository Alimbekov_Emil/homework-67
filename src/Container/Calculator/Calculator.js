import React from "react";
import "./Calculator.css";
import { useDispatch, useSelector } from "react-redux";

const Calculator = () => {
  const dispath = useDispatch();

  const state = useSelector((state) => state);

  const changeNumber = (number) => dispath({ type: "NUMBER", value: number });
  const equally = () => dispath({ type: "EQUALLY" });
  const inputChange = (e) => dispath({ type: "CHANGE", value: e.target.value });
  const clearChange = () => dispath({ type: "CLEAR" });

  const btnNumber = state.numbers.map((number) => {
    return (
      <button
        key={number}
        className="btn"
        id={number}
        onClick={() => changeNumber(number)}
      >
        {number}
      </button>
    );
  });
  return (
    <div className="Calculator">
      <input type="text" value={state.inputValue} onChange={(e) => inputChange(e)} />
      <div className="CalculatorNumber">
        <button className="btn" onClick={clearChange}>
          C
        </button>

        {btnNumber}
        <button className="btn" onClick={equally}>
          =
        </button>
      </div>
    </div>
  );
};

export default Calculator;
